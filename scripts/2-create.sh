#!/bin/bash
set -e 
rm -rf gentoo out
mkdir gentoo
mkdir out
mkdir out/lib
mkdir out/bin
chmod -R 755 out
tar xfp hardened.tar.xz -C gentoo
