#!/bin/bash
set -e

LATEST=$( curl -L "https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/latest-stage3-amd64-hardened-openrc.txt" | tail -n 1 | awk '{print $1}' )
wget "https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/$LATEST" -O hardened.tar.xz
wget "https://bouncer.gentoo.org/fetch/root/all/releases/amd64/autobuilds/$LATEST.asc" -O hardened.tar.xz.asc
gpg --verify hardened.tar.xz.asc hardened.tar.xz



