# looqs-bundle

The aim of this project is to create a tarball for [looqs](https://github.com/quitesimpleorg/looqs) that runs on any (recent)
Linux distribution.

It uses [Gentoo Hardened](https://wiki.gentoo.org/wiki/Hardened_Gentoo) and creates a Qt library with less features than what is 
usually found in distributions.
